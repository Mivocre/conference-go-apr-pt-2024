from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            dict = {}
            if hasattr(o, "get_api_url"):
                dict["href"] = o.get_api_url()
            for prop in self.properties:
                value = getattr(o, prop)
                if prop in self.encoders:
                    encoder = self.encoders[prop]
                    value = encoder.default(value)
                dict[prop] = value
            dict.update(self.get_extra_data(o))
            return dict
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


# class OneEncoderToRuleThemAll(JSONEncoder):
#     def default(self, o):
#         print(o)
#         if isinstance(o, QuerySet):
#             return list(o)
#         elif isinstance(o, Project):
#             return {
#                 "id": o.id,
#                 "name": o.name,
#                 "description": o.description,
#                 "owner": o.owner,
#                 "tasks": o.tasks.all(),
#             }
#         elif isinstance(o, Task):
#             return {
#                 "id": o.id,
#                 "start_date": o.start_date,
#                 "due_date": o.due_date,
#                 "is_completed": o.is_completed,
#             }
#         elif isinstance(o, User):
#             return {"id": o.pk, "username": o.username}
#         elif isinstance(o, datetime):
#             return o.isoformat()
#         else:
#             return super().default(o)

#   match (o):
#     case QuerySet():
#         return list(o)
#     case Project():
#         return {
#             "id": o.pk,
#             "name": o.name,
#             "description": o.description,
#             "owner": o.owner,
#         }
#     case Task():
#         return {
#             "id": o.pk,
#             "start_date": o.start_date,
#             "due_date": o.due_date,
#             "is_completed": o.is_completed,
#         }
#     case User():
#         return {"id": o.pk, "username": o.username}
#     case datetime():
#         return o.isoformat()
#     case _:
#         super().default(o)
